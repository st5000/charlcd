#!/usr/bin/python
# -*- coding: utf-8 -*-
#pylint: skip-file

"""Tests for charlcd.lcd"""

__author__ = 'Bartosz Kościów'

from mock import call
from mock import MagicMock
from nose.tools import assert_equal
from nose.tools import assert_raises
from charlcd.drivers.null import Null
from charlcd import virtual_buffered
from charlcd import buffered


class TestLcdVirtualBuffered(object):
    def setUp(self):
        self.screen = virtual_buffered.CharLCD(20, 6)
        self.lcd1 = buffered.CharLCD(20, 4, Null())
        self.lcd1.init()
        self.lcd2 = buffered.CharLCD(16, 2, Null())
        self.lcd2.init()
        self.screen.add_display(0, 0, self.lcd1)
        self.screen.add_display(2, 4, self.lcd2)
        self.screen.init()
        self.buffer = [" ".ljust(20, " ") for i in range(0, 6)]

    def test_init(self):
        assert_equal(self.screen.get_width(), 20)
        assert_equal(self.screen.get_height(), 6)
        assert_equal(self.screen.get_display_mode(), 'buffered')

    def test_buffer_should_be_empty_after_init(self):
        self.screen.init()
        assert_equal(self.screen.buffer, self.buffer)

    def test_write_will_hit_proper_lcd(self):
        self.screen.write("A", 0, 0)
        assert_equal(self.screen.buffer[0][0], 'A')
        assert_equal(self.lcd1.buffer[0][0], 'A')
        assert_equal(self.screen.buffer[0][0], self.lcd1.buffer[0][0])

    def test_write_will_hit_proper_screen_second(self):
        self.screen.write("G", 17, 5)
        assert_equal(self.screen.buffer[5][17], 'G')
        assert_equal(self.lcd2.buffer[1][15], 'G')
        assert_equal(self.screen.buffer[5][17], self.lcd2.buffer[1][15])

    def test_set_xy_will_equal_get_xy(self):
        self.screen.set_xy(10,3)
        xy = self.screen.get_xy()
        assert_equal(xy['x'], 10)
        assert_equal(xy['y'], 3)

    def test_set_xy_will_raise_exception(self):
        assert_raises(IndexError, self.screen.set_xy, 40, 10)

    def test_flush_will_flush_screens(self):
        self.lcd1.flush = MagicMock()
        self.lcd2.flush = MagicMock()
        self.screen.flush()
        assert_equal(self.lcd1.flush.call_count, 1)
        assert_equal(self.lcd2.flush.call_count, 1)

    def test_buffer_clear_call_subfunctions(self):
        self.lcd1.buffer_clear = MagicMock()
        self.lcd2.buffer_clear = MagicMock()
        self.screen.buffer_clear()
        assert_equal(self.lcd1.buffer_clear.call_count, 1)
        assert_equal(self.lcd2.buffer_clear.call_count, 1)

    # def test_buffer_clear_area(self):
    #     screen = lcd_virtual_buffered.CharLCD(10, 4)
    #     screen.init()
    #     lcd1 = lcd_buffered.CharLCD(20, 4, Null())
    #     lcd1.init()
    #     lcd2 = lcd_buffered.CharLCD(16, 2, Null())
    #     lcd2.init()
    #     screen.add_display(0, 0, lcd1, 5, 2)
    #     screen.add_display(0, 2, lcd2, 5, 0)
    #     buffer = [
    #         "qwertyuiop",
    #         "asd    kla",
    #         "zxc    zxc",
    #         "1234567890"
    #     ]
    #     screen.write("qwertyuiop")
    #     screen.write("asdfghjkla", 0, 1)
    #     screen.write("zxcvbnmzxc", 0, 2)
    #     screen.write("1234567890", 0, 3)
    #
    #     screen.buffer_clear(3, 1, 4, 2)
    #
    #     print lcd1.buffer
    #     print lcd2.buffer
    #     assert_equal(screen.buffer, buffer)
    #     assert_equal(lcd1.buffer[3], "     asd    kla    ")
    #     assert_equal(lcd2.buffer[0], "     zxc    zxc '")

