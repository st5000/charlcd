#!/usr/bin/python
# -*- coding: utf-8 -*-
#pylint: skip-file

"""Tests for charlcd.lcd"""

__author__ = 'Bartosz Kościów'

from mock import call
from mock import MagicMock
from nose.tools import assert_equal
from nose.tools import assert_raises
from charlcd import buffered
from charlcd.drivers.null import Null
from charlcd.drivers.null_events import NullEvents


class TestLcd(object):
    def setUp(self):
        self.screen = buffered.CharLCD(20, 4, Null())
        self.buffer = [" ".ljust(20, " ") for i in range(0, 4)]

    def test_init(self):
        assert_equal(self.screen.get_width(), 20)
        assert_equal(self.screen.get_height(), 4)
        assert_equal(self.screen.get_display_mode(), 'buffered')

    def test_screen_should_be_empty_after_init(self):
        self.screen.init()
        assert_equal(self.screen.screen, self.buffer)

    def test_buffer_should_have_content_after_write(self):
        self.screen.init()
        self.screen.write("string", 0, 0)
        self.output = [" ".ljust(20, " ") for i in range(0, 4)]
        self.output[0] = "string" + " ".ljust(14, " ")
        assert_equal(self.screen.buffer, self.output)

    def test_buffer_should_have_content_after_write_on_last_width(self):
        self.screen.init()
        self.screen.write("s", 19, 0)
        self.output = [" ".ljust(20, " ") for i in range(0, 4)]
        self.output[0] = " ".ljust(19, " ") + "s"
        assert_equal(self.screen.buffer, self.output)

    def test_buffer_should_have_content_after_2nd_write(self):
        self.screen.init()
        self.screen.write("string", 0, 0)
        self.screen.write("string", 1, 1)
        self.output = [" ".ljust(20, " ") for i in range(0, 4)]
        self.output[0] = "string" + " ".ljust(14, " ")
        self.output[1] = " string" + " ".ljust(13, " ")
        assert_equal(self.screen.buffer, self.output)

    def test_buffer_should_have_content_after_default_position_write(self):
        self.screen.init()
        self.screen.write("string", 0, 0)
        self.screen.write("word")
        self.output = [" ".ljust(20, " ") for i in range(0, 4)]
        self.output[0] = "stringword" + " ".ljust(10, " ")
        assert_equal(self.screen.buffer, self.output)

    def test_write_height_out_of_range_should_return_exception(self):
        self.screen.init()
        assert_raises(IndexError, self.screen.write, 'word', 10, 10)

    def test_write_width_out_of_range_should_return_exception(self):
        self.screen.init()
        assert_raises(IndexError, self.screen.write, 'string', 40, 0)

    def test_set_xy_should_change_position(self):
        self.screen.init()
        self.screen.set_xy(5, 2)
        assert_equal(self.screen.get_xy(), {'x': 5, 'y': 2})

    def test_corrent_no_of_calls_while_flushing_first_time(self):
        self.screen.init()
        line1 = '-  Blarg'
        self.screen.write(line1)
        self.screen.driver.cmd = MagicMock()
        self.screen.driver.char = MagicMock()
        self.screen.flush()
        assert_equal(self.screen.driver.cmd.call_args_list, [
            call(128, 0),
            call(131, 0)
        ])
        assert_equal(self.screen.driver.char.call_args_list, [
            call('-', 0), call('B', 0), call('l', 0), call('a', 0), call('r', 0), call('g', 0)
        ])

    def test_corrent_no_of_calls_while_flushing_second_time(self):
        self.screen.init()
        line1 = '-  Blarg'
        self.screen.write(line1)
        self.screen.flush()

        line1 = '-  blarg!'
        self.screen.write(line1, 0, 0)
        self.screen.driver.cmd = MagicMock()
        self.screen.driver.char = MagicMock()
        self.screen.flush()

        assert_equal(self.screen.driver.cmd.call_args_list, [
            call(131, 0),
            call(136, 0)
        ])
        assert_equal(self.screen.driver.char.call_args_list, [
            call('b', 0), call('!', 0)
        ])

    def test_clear_buffer_should_clear_area(self):
        self.screen.init()
        line = '12345678901234567890'
        line1 = '12345678901234567890'
        line2 = '12345     1234567890'
        line3 = '12345     1234567890'
        line4 = '12345678901234567890'
        self.screen.write(line, 0, 0)
        self.screen.write(line, 0, 1)
        self.screen.write(line, 0, 2)
        self.screen.write(line, 0, 3)
        self.screen.buffer_clear(5, 1, 5, 2)

        output = [line1, line2, line3, line4]
        assert_equal(self.screen.buffer, output)

    def test_clear_buffer_should_clear_default_area(self):
        self.screen.init()
        line = '12345678901234567890'
        line1 = '12345678901234567890'
        line2 = '12345               '
        line3 = '12345               '
        line4 = '12345               '
        self.screen.write(line, 0, 0)
        self.screen.write(line, 0, 1)
        self.screen.write(line, 0, 2)
        self.screen.write(line, 0, 3)
        self.screen.buffer_clear(5, 1)

        output = [line1, line2, line3, line4]
        assert_equal(self.screen.buffer, output)

    def test_it_should_call_proper_e_line(self):
        drv = Null()
        drv.pins['E2'] = 10
        screen = buffered.CharLCD(40, 4, drv)
        assert_equal(screen._get_enable(1), 0)
        assert_equal(screen._get_enable(3), 1)

    def test_without_event_interface_no_event_is_called(self):
        drv = Null()
        drv.pre_flush = MagicMock()
        drv.post_flush = MagicMock()
        screen = buffered.CharLCD(16, 2, drv)
        screen.init()
        screen.flush()
        screen.write('abc', 0, 0)
        drv.pre_flush.assert_not_called()
        drv.post_flush.assert_not_called()

    def test_it_should_call_flush_events(self):
        drv = NullEvents()
        line = '12345678901234567890'
        drv.pre_flush = MagicMock()
        drv.post_flush = MagicMock()
        screen = buffered.CharLCD(16, 2, drv)
        screen.init()
        screen.write(line, 0, 0)
        screen.flush()
        drv.pre_flush.assert_called_once()
        drv.post_flush.assert_called_once()



