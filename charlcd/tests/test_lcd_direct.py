#!/usr/bin/python
# -*- coding: utf-8 -*-
#pylint: skip-file

"""Tests for charlcd.lcd"""

__author__ = 'Bartosz Kościów'

from nose.tools import assert_equal
from charlcd import direct
from charlcd.drivers.null import Null


class TestLcd(object):
    def test_init(self):
        screen = direct.CharLCD(20, 4, Null())
        assert_equal(screen.get_width(), 20)
        assert_equal(screen.get_height(), 4)
        assert_equal(screen.get_display_mode(), 'direct')

    def test_stream(self):
        screen = direct.CharLCD(8, 2, Null())
        assert_equal(screen.current_pos['x'], 0)
        assert_equal(screen.current_pos['y'], 0)

        screen.stream("t")
        assert_equal(screen.current_pos['x'], 1)
        assert_equal(screen.current_pos['y'], 0)

    def test_stream_should_break_line(self):
        screen = direct.CharLCD(2, 2, Null())
        screen.stream("t")
        screen.stream("t")

        assert_equal(screen.current_pos['x'], 0)
        assert_equal(screen.current_pos['y'], 1)

    def test_stream_should_return_top(self):
        screen = direct.CharLCD(2, 2, Null())
        screen.stream("tt")
        screen.stream("tt")

        assert_equal(screen.current_pos['x'], 0)
        assert_equal(screen.current_pos['y'], 0)
