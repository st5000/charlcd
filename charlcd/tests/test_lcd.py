#!/usr/bin/python
# -*- coding: utf-8 -*-
#pylint: skip-file

"""Tests for charlcd.lcd"""

__author__ = 'Bartosz Kościów'

from nose.tools import assert_equal

from charlcd.abstract import lcd
from charlcd.drivers.null import Null


class TestLcd(object):
    def test_init(self):
        screen = lcd.CharLCD(20, 4, Null())
        assert_equal(screen.get_width(), 20)
        assert_equal(screen.get_height(), 4)
        assert_equal(screen.get_display_mode(), 'direct')

    def test_init_buffered_mode(self):
        screen = lcd.CharLCD(20, 4, Null(), lcd.DISPLAY_MODE_BUFFERED)
        assert_equal(screen.get_width(), 20)
        assert_equal(screen.get_height(), 4)
        assert_equal(screen.get_display_mode(), 'buffered')

    def test_init_lcd_without_cursor_and_blinking(self):
        screen = lcd.CharLCD(20, 4, Null(), cursor_visible=0, cursor_blink=0)
        assert_equal(screen.cursor_visible, 0)
        assert_equal(screen.cursor_blink, 0)
